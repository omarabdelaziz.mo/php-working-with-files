<?php

include_once 'utility.php';

$activityId = $_GET["id"];
$activity = new Activity();
$activity = $activity->find($activityId);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Activity Details</title>
</head>
<body>
    <h1>Activity Details</h1>
    <section>
        <h2>Activity Name: <?php echo $activity->name; ?></h2>
        <h2>Activity Time: <?php echo $activity->time; ?></h2>
    </section>
    <section>
        <ul>
            <?php foreach ($activity->activityDetails() as $activityDetail) { ?>
                <li>
                    <?php echo $activityDetail->user->name; ?> | <?php echo $activityDetail->quality; ?> | <?php echo $activityDetail->time; ?>
                </li>
    
            <?php } ?>
    <section>
        <a href="editActivity.php?id=<?php echo $activity->id; ?>">Edit</a> | <a href="deleteActivity.php?id=<?php echo $activity->id; ?>">Delete</a>
    </section>

</body>
</html>
