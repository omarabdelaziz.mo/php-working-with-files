<!DOCTYPE html>
<html>
<head>
    <title>List</title>
</head>
<body>
    <?php 
        include_once "utility.php";
        $activity = new Activity();
        $activities = $activity->all();
    ?>
    <h1>Activities</h1>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Time</th>
            <th>Action</th>
        </tr>
        <?php foreach ($activities as $activity) { ?>
            <tr>
                <td><?php echo $activity->id; ?></td>
                <td><a href="activityDetails.php?id=<?php echo $activity->id; ?>"><?php echo $activity->name; ?></a></td>
                <td><?php echo $activity->time; ?></td>
                <td>
                    <a href="editActivity.php?id=<?php echo $activity->id; ?>" style="text-decoration: none;">Edit</a> | <a href="deleteActivity.php?id=<?php echo $activity->id; ?>" style="text-decoration: none;">Delete</a>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td></td>
            <td></td>
                <td><a href="addActivity.html" style="text-decoration: none;">Add Activity</a></td>
        </tr>
    </table>
    
</body>
</html>
