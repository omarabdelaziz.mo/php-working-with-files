<?php

class ActivityDetails
{
    public $id;
    public $userId;
    public $activityId;
    public $time;
    public $quality;
    public $user;
    public $activity;
    public $fileManager;
    public $columns = [
        "id" => 0,
        "userId" => 1,
        "activityId" => 2,
        "time" => 3,
        "quality" => 4
    ];


    public function __construct($id = null, $userId = null, $activityId = null, $time = null, $quality = null)
    {
        $this->fileManager = new FileManager('activityDetails.txt', '~');
        $this->id = $id;
        $this->userId = $userId;
        $this->activityId = $activityId;
        $this->time = $time;
        $this->quality = $quality;
        
    }

    public function find($id)
    {
        $activityDetails = $this->fileManager->find($id);
        $this->id = $activityDetails[0];
        $this->userId = $activityDetails[1];
        $this->activityId = $activityDetails[2];
        $this->time = $activityDetails[3];
        $this->quality = $activityDetails[4];
        $this->user = (new User())->find($this->userId);
        $this->activity = (new Activity())->find($this->activityId);
        return $this;
    }

    public function where($column, $value)
    {
        $details = $this->fileManager->where($this->columns[$column], $value);
        $activityDetails = array();
        foreach ($details as $activityDetail) {
            $activityDetail = (new ActivityDetails())->find($activityDetail[0]);
            array_push($activityDetails, $activityDetail);
        }
        return $activityDetails;
    }


}
class Activity
{
    public $id;
    public $name;
    public $time;
    public FileManager $fileManager;
    public $activityDetails;
    public $columns = [
        'id' => 0,
        'name' => 2,
        'time' => 3
    ];

    public function __construct($name = null, $time = null)
    {
        $this->name = $name;
        $this->time = $time;
        $this->fileManager = new FileManager("activity.txt", "~");
    }

    public function __toString()
    {
        return $this->id . " " . $this->name . " " . $this->time;
    }

    public function save()
    {
        $lastId = $this->fileManager->getLastID();
        $newId = $lastId + 1;
        $record = $newId . $this->fileManager->getSeparator() . $this->name . $this->fileManager->getSeparator() . $this->time;
        $this->fileManager->store($record);
    }

    public function find($id)
    {
        $record = $this->fileManager->find($id);
        $this->id = $record[0];
        $this->name = $record[1];
        $this->time = $record[2];

        return $this;
    }

    public function all()
    {
        $records = $this->fileManager->all();
        $activities = array();
        foreach ($records as $record) {
            $activity = new Activity();
            $activity->id = $record[0];
            $activity->name = $record[1];
            $activity->time = $record[2];
            array_push($activities, $activity);
        }

        return $activities;
    }

    public function delete($id)
    {
        $this->fileManager->delete($id);
    }

    public function activityDetails()
    {
        $activityDetails = new ActivityDetails();
        $this->activityDetails = $activityDetails->where('activityId', $this->id);
        return $this->activityDetails;
    }

}

class User
{
    public $id;
    public $name;
    public $email;
    public $password;
    public $gender;
    public FileManager $fileManager;

    public function __construct($name = null, $email = null, $password = null)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->fileManager = new FileManager("user.txt", "~");
    }

    public function __toString()
    {
        return $this->id . " " . $this->name . " " . $this->email . " " . $this->password;
    }

    public function login($email, $password)
    {
        $user = $this->findOneBy('email', $email);
        if ($user != null) {
            if ($user->password == $password) {
                return $user;
            }
        }
        return null;
    }

    public function save()
    {
        $lastId = $this->fileManager->getLastID();
        $newId = $lastId + 1;
        $record = "$newId~$this->name~$this->email~$this->password";
        $this->fileManager->store($record);
    }

    public function find($id)
    {
        $record = $this->fileManager->find($id);
        $this->id = $record[0];
        $this->name = $record[1];
        $this->email = $record[2];
        $this->password = $record[3];

        return $this;
    }

    public function all()
    {
        $records = $this->fileManager->all();
        $users = array();
        foreach ($records as $record) {
            $user = new User();
            $user->id = $record[0];
            $user->name = $record[1];
            $user->email = $record[2];
            $user->password = $record[3];
            array_push($users, $user);
        }

        return $users;
    }
    
    public function findOneBy($column, $value)
    {
        $record = $this->fileManager->findOneBy($this->columns[$column], $value);
        $this->id = $record[0];
        $this->name = $record[1];
        $this->email = $record[2];
        $this->password = $record[3];
        $this->gender = $record[4];

        return $this;
    }

    public function delete($id)
    {
        $this->fileManager->delete($id);
    }
}
class FileManager
{
    public $fileName;
    public $separator;

    public function __construct($fileName, $separator)
    {
        $this->fileName = $fileName;
        $this->separator = $separator;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function getSeparator()
    {
        return $this->separator;
    }

    public function getLastID()
    {
        if (!file_exists($this->fileName)) {
            return 0;
        }
        $file = fopen($this->fileName, "r") or die("Unable to open file!");
        $lastId = 0;
        while (!feof($file)) {
            $line = fgets($file);
            $line = trim($line);
            if ($line != "") {
                $line = explode($this->separator, $line);
                $lastId = $line[0];
            }
        }
        fclose($file);
        return $lastId;
    }

    public function store($record)
    {
        $file = fopen($this->fileName, "a+") or die("Unable to open file!");
        fwrite($file, $record . "\r\n");
        fclose($file);
    }

    public function find($id)
    {
        $file = fopen($this->fileName, "r") or die("Unable to open file!");
        $record = array();
        while (!feof($file)) {
            $line = fgets($file);
            $line = trim($line);
            if ($line != "") {
                $line = explode($this->separator, $line);
                if ($line[0] == $id) {
                    $record = $line;
                    break;
                }
            }
        }
        fclose($file);
        return $record;
    }

    public function findOneBy($column, $value)
    {
        $file = fopen($this->fileName, "r") or die("Unable to open file!");
        $record = array();
        while (!feof($file)) {
            $line = fgets($file);
            $line = trim($line);
            if ($line != "") {
                $line = explode($this->separator, $line);
                if ($line[$this->columns[$column]] == $value) {
                    $record = $line;
                    break;
                }
            }
        }
        fclose($file);
        return $record;
    }

    public function all()
    {
        $file = fopen($this->fileName, "r") or die("Unable to open file!");
        $records = array();
        while (!feof($file)) {
            $line = fgets($file);
            $line = trim($line);
            if ($line != "") {
                $line = explode($this->separator, $line);
                $records[] = $line;
            }
        }
        fclose($file);
        return $records;
    }

    public function delete($id)
    {
        $file = fopen($this->fileName, "r") or die("Unable to open file!");
        $tempFile = fopen("temp.txt", "w") or die("Unable to open file!");
        while (!feof($file)) {
            $line = fgets($file);
            $line = trim($line);
            if ($line != "") {
                $line = explode($this->separator, $line);
                if ($line[0] != $id) {
                    fwrite($tempFile, $line[0] . $this->separator . $line[1] . $this->separator . $line[2] . "\r\n");
                }
            }
        }
        fclose($file);
        fclose($tempFile);
        unlink($this->fileName);
        rename("temp.txt", $this->fileName);
    }

    public function where($column, $value)
    {
        $file = fopen($this->fileName, "r") or die("Unable to open file!");
        $records = array();
        while (!feof($file)) {
            $line = fgets($file);
            $line = trim($line);
            if ($line != "") {
                $line = explode($this->separator, $line);
                if ($line[$column] == $value) {
                    $records[] = $line;
                }
            }
        }
        fclose($file);
        return $records;
    }
}
